FROM python:3.10.8-bullseye

WORKDIR /app/cli

COPY dashboard /app/cli/dashboard

RUN pip3 install grafana-dashboard-manager